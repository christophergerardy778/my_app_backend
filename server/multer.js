const multer = require("multer");
const path = require("path");
const uuidV4 = require("uuid/v4");

const storage = multer.diskStorage({
   destination: path.resolve(__dirname, "uploads/"),

    filename: (req, file, cb) => {
        cb(null, uuidV4() + path.extname(file.originalname));
    },
});

module.exports = storage;