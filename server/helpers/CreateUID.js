const uuidV4 = require("uuid/v4");

class CreateUID {
    static async create(name, lastName) {
        return `${name.toLowerCase()}-${lastName.toLowerCase()}-${uuidV4()}`;
    }
}

module.exports = CreateUID;