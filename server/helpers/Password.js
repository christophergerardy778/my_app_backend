const bcryptjs = require("bcryptjs");

class Password {
    static async generatePassword(password) {
        const salt = await bcryptjs.genSaltSync(10);
        return bcryptjs.hashSync(password, salt);
    }

    static async comparePassword(password, hash) {
        return bcryptjs.compareSync(password, hash);
    }
}

module.exports = Password;