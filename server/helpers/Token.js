const jwt = require("jsonwebtoken");

class Token {
    static SIGN() {
        return "GEO699";
    }

    static async create(myJson) {
        delete myJson.password;
        return jwt.sign(myJson, this.SIGN());
    }
}

module.exports = Token;