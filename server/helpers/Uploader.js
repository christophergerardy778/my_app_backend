const { API_KEY, API_SECRET, CLOUD_NAME } = require("../env");
const cloudinary = require("cloudinary").v2;
const fs = require("fs-extra");

cloudinary.config({
    cloud_name: CLOUD_NAME,
    api_key: API_KEY,
    api_secret: API_SECRET
});

class Uploader {
    static async picture(file) {
        return cloudinary.uploader.upload(file);
    }

    static async cleanStorage(filePath) {
        fs.unlink(filePath);
    }

    static async video(file) {
        return cloudinary.uploader.upload(file, {
            resource_type: "video"
        });
    }
}

module.exports = Uploader;