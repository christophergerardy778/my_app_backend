class Response {
    static Send(res, data) {
	res.set('Cache-Control', 'public, max-age=300')
        return res.send({
            ok: true,
            data
        });
    }

    static Error(res, error) {
        return res.send({
            ok: false,
            error
        });
    }
}

module.exports = Response;
