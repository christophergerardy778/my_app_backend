const Model = require("../models/index");
const Response = require("../helpers/Response");
const Token = require("../helpers/Token");
const Password = require("../helpers/Password");

class LoginController {
    static async login(req, res) {
        try {
            const { email, password } = req.body;
            const result = await Model.User.findAndCountAll({ where: { email } });

            if (result.count < 1) return Response.Error(res, "Email no existe");

            const user = result.rows[0].dataValues; // JSON USER
            const correctPassword = await Password.comparePassword(password, user.password);

            if (!correctPassword) return Response.Error(res, "Credenciales incorrectas");
            const token = await Token.create(user);

            Response.Send(res, token);

        } catch (e) {
            Response.Error(res, "" + e);
        }
    }
}

module.exports = LoginController;