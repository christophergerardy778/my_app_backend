const CreateUID = require("../helpers/CreateUID");
const Response = require("../helpers/Response.js");
const Password = require("../helpers/Password");
const Token = require("../helpers/Token");
const Model = require("../models/index");

class RegisterController {
    static async register(req, res) {
        try {
            const { name, lastName, gender, accountType, email, password } = req.body;
            const result = await Model.User.findAndCountAll({ where: { email } });

            if (result.count > 0) return Response.Error(res, "Email en uso");

            const User = await Model.User.create({
                name,
                lastName,
                gender,
                accountType,
                email,
                password: await Password.generatePassword(password),
                userName: await CreateUID.create(name, lastName)
            });

            const cart = await Model.ShoppingCart.create({
                userId: User.dataValues.id
            });

            await Model.User.update(
                { cartId: cart.dataValues.id },
                { where: { id: User.dataValues.id } }
            );

            const UserWithCart = await Model.User.findOne({
                where: { id: User.dataValues.id }
            });

            const token = await Token.create(UserWithCart.dataValues);
            Response.Send(res, token);

        } catch (e) {
            Response.Error(res, "Error: " + e);
        }
    }
}

module.exports = RegisterController;