const stripe = require("stripe")("sk_test_FRAGazIzSkBIYOBY1yQJsEMm00GX3ehMMI");
const Response = require("../helpers/Response");
const Model = require("../models/index");

class PaymentController {
    static async createIntent(req, res) {
        let amount = 0;

        const { shoppingCartId } = req.body;

        const cart = await Model.InShoppingCart.findAll({
            where: { shoppingCartId },
            include: [Model.Course]
        });

        for (let i = 0; i <cart.length ; i++) {
            amount += (cart[i].dataValues.Course.price * 100);
        }

        const intent = await stripe.paymentIntents.create({
            amount: amount,
            currency: 'mxn'
        });

        const { client_secret } = intent;

        Response.Send(res, client_secret);
    }

    static async savePaymentIntent(req, res) {
        try {
            const { paymentIntent, shoppingCartId } = req.body;

            if (paymentIntent != null && shoppingCartId != null) {

                await Model.PaymentIntent.create({
                    paymentIntent,
                    shoppingCartId
                });

                Response.Send(res, true);
            }
        } catch (e) {
            Response.Error(res, e.toString());
        }
    }

    static async webHook(req, res) {
        switch (req.body.type) {
            case 'payment_intent.succeeded':
                await PaymentController.updateCourseLists(req.body.data.object.client_secret);
                console.log('PaymentIntent was successful!');
                break;
            case 'payment_method.attached':
                console.log('PaymentMethod was attached to a Customer!');
                break;
            default:
                return res.status(400).end();
        }
        res.json({received: true});
    }

    static async updateCourseLists(paymentIntent) {
        const result = await Model.PaymentIntent.findOne({ where: { paymentIntent } });
        const shoppingCartId = result.dataValues.shoppingCartId;

        const cart = await Model.ShoppingCart.findOne({ where: { id: shoppingCartId } });
        const userId = cart.dataValues.userId;

        const courses = await Model.InShoppingCart.findAll({ where: { shoppingCartId }, raw: true });

        for (let i = 0; i <courses.length ; i++) {
            await Model.payment.create({
                userId,
                courseId: courses[i].courseId
            });

            await Model.InShoppingCart.destroy({
                where: { shoppingCartId, courseId: courses[i].courseId }
            });
        }
    }
}

module.exports = PaymentController;