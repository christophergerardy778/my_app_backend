const Response = require("../helpers/Response");
const Model = require("../models/index");

class ShoppingCartController {

    static async addToShoppingCard(req, res) {
        try {
            const { shoppingCartId, courseId } = req.body;
            const inShoppingCard = await Model.InShoppingCart.findOne({where: { shoppingCartId, courseId }});

            if (inShoppingCard) return Response.Error(res, "Ya esta en el carro");

            await Model.InShoppingCart.create({ shoppingCartId, courseId });

            Response.Send(res, "Curso agregado al carrito")
        } catch (e) {
            Response.Error(res, e.toString());
        }
    }

    static async removeFromShoppingCart(req, res) {
        try {
            const { shoppingCartId, courseId } = req.body;
            await Model.InShoppingCart.destroy({ where: { shoppingCartId, courseId } });
            Response.Send(res, "Curso eliminado del carrito")
        } catch (e) {
            Response.Error(res, e.toString());
        }
    }

    static async getShoppingCart(req, res) {
        try {
            const { userId } = req.params;

            if (userId) {
                const cart = await Model.ShoppingCart.findOne({
                    where: { userId },
                    include: [
                        {
                            model: Model.InShoppingCart, include: [
                                { model: Model.Course, include: [Model.CourseImg] }
                            ]
                        }
                    ]
                });

                let arr = [];

                for (let i = 0; i <cart.dataValues.InShoppingCarts.length ; i++) {
                    arr.push(cart.dataValues.InShoppingCarts[i].Course);
                }

                Response.Send(res, arr);
            }

        } catch (e) {
            console.log(e);
            Response.Error(res, e.toString());
        }
    }
}

module.exports = ShoppingCartController;