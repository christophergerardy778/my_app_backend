const Response = require("../helpers/Response");
const Model = require("../models/index");
const Uploader = require("../helpers/Uploader");

class VideoController {
    static async createVideo(req, res) {
        try {
            const { sectionId, name } = req.body;
            const videoExists = await Model.Video.findOne({ where: { sectionId, name } });
            if (videoExists) return Response.Error(res, "Esta seccion ya tiene un video con este nombre");

            const { public_id, secure_url } = await Uploader.video(req.file.path);
            await Uploader.cleanStorage(req.file.path);

            await Model.Video.create({
                sectionId,
                name,
                publicId: public_id,
                url: secure_url
            });

            Response.Send(res, "Video creado correctamente")
        } catch (e) {
            Response.Error(res, e.toString());
        }
    }
}

module.exports = VideoController;