const Model = require("../models/index");
const Response = require("../helpers/Response");
const Uploader = require("../helpers/Uploader");
const Op = require("sequelize").Op;

class CourseController {
    static async getAllCourses(req, res) {
        try {

            const BestRate = await Model.Course.findAll({
                order: [ ["rate", "DESC"] ],
                limit: 8,
                include: [Model.CourseImg, Model.Category]
            });

            const Development = await Model.Course.findAll({
                where: { categoryId: 1 },
                order: [ ["categoryId", "DESC"] ],
                limit: 8,
                include: [Model.CourseImg]
            });

            const Design = await Model.Course.findAll({
                where: { categoryId: 2 },
                order: [ ["rate", "DESC"] ],
                limit: 8,
                include: [Model.CourseImg, Model.Category]
            });

            const Mathematics = await Model.Course.findAll({
                where: { categoryId: 3 },
                order: [ ["rate", "DESC" ] ],
                limit: 8,
                include: [Model.CourseImg]
            });

            const sections = [BestRate, Development, Design, Mathematics];
            const titles = ["Mejor valorados", "Desarollo", "Diseño", "Matematicas"];
            const result = await CourseController.createSectionsList(sections, titles);

            Response.Send(res, result);

        } catch (e) {
            Response.Error(res, e.toString());
        }
    }

    static async createCourse(req, res) {
        try {
            let { title, description, instructorId, price, categoryId } = req.body;

            const sameName = await Model.Course.findOne({ where: { instructorId, title } });
            if (sameName) return Response.Error(res, "Ya tienes un curso con este nombre");

            const course = await Model.Course.create({
                title,
                description,
                instructorId,
                price,
                categoryId,
                students: 0,
                rate: 0,
                imgId: 1
            });

            if (req.file) {
                const { public_id, secure_url } = await Uploader.picture(req.file.path);
                const courseId = course.dataValues.id;

                const img = await Model.CourseImg.create({
                   courseId,
                   publicId: public_id,
                   url: secure_url
                });

                await Model.Course.update(
                    { imgId: img.dataValues.id },
                    { where: { id: courseId  } }
                );

                await Uploader.cleanStorage(req.file.path);
            }

            Response.Send(res, "Curso creado")

        } catch (e) {
            Response.Error(res, e.toString());
        }
    }

    static async infoCourse(req, res)  {
        try {
            const { userId, courseId } = req.params;
            const course = await Model.payment.findOne({ where: { courseId, userId } });

            if (course) {
                const coursePayed = await Model.Course.findOne({ where: { id: courseId },
                    include: [
                        { model: Model.Category },
                        { model: Model.User },
                        { model: Model.CourseImg },
                        { model: Model.Section, include: [{ model: Model.Video }] }
                    ]
                });

                return Response.Send(res, {
                    payed: true,
                    course: coursePayed
                });
            }

            const courseNoPayed = await Model.Course.findOne({ where: { id: courseId },
                include: [
                    { model: Model.Category },
                    { model: Model.User },
                    { model: Model.CourseImg },
                    { model: Model.Section, include: [{ model: Model.Video }] }
                ]
            });

            Response.Send(res, {
                payed: false,
                course: courseNoPayed
            })

        } catch (e) {
            Response.Error(res, e.toString());
        }
    }

    static async createSectionsList(sections, titles) {
        const result = [];

        for (let i = 0; i <sections.length ; i++) {
            result.push({
                title: titles[i],
                courses: sections[i]
            });
        }

        return result;
    }

    static async searchCourse(req, res) {
        try {
            const { query } = req.query;

            const result = await Model.Course.findAll({
                where: {
                    title: { [Op.like]: `${query}%` }
                },

                limit: 10,
                include: [Model.CourseImg]
            });

            Response.Send(res, result);

        } catch (e) {
            Response.Error(res, e.toString());
        }
    }

    static async checkCourse(req, res) {
        try {
            const { userId, courseId } = req.params;
            const course = await Model.payment.findOne({ where: { courseId, userId } });

            if (!course) return Response.Send(res, false);

            return Response.Send(res, true);
            
        } catch (e) {
            Response.Error(res, e.toString());
        }
    }
    
    static async getMyCourses(req, res) {
        try {
            const arr = [];
            const {userId} = req.params;

            const courses = await Model.payment.findAll({ where: { userId } });

            for (let i = 0; i <courses.length ; i++) {
                arr.push(courses[i].dataValues.courseId);
            }

            const courseList = await Model.Course.findAll({ where: { id: arr },
                include: [
                    { model: Model.Category },
                    { model: Model.User },
                    { model: Model.CourseImg },
                    { model: Model.Section, include: [{ model: Model.Video }] }
                ]
            });

            Response.Send(res, courseList);
            
        } catch (e) {
            Response.Error(res, e.toString());
        }
    }

    static async addSection(req, res) {
        try {
            await Model.Section.create(req.body);
            Response.Send(res, "Creado");
        } catch (e) {
            Response.Error(res, e);
        }
    }
}

module.exports = CourseController;