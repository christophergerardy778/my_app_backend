const Router = require("express").Router();
const multer = require("multer");
const storage = require("../multer");
const VideoController = require("../controllers/VideoController");

Router.route("/video").post(
    multer({ storage }).single("video"),
    VideoController.createVideo );


module.exports = Router;