const auth = require("./Auth");
const Courses = require("./Course");
const Video = require("./Video");
const Payment = require("./Stripe");
const ShoppingCart = require("./ShoppingCart");

const express = require("express");
const app = express();

app.use(auth);
app.use(Courses);
app.use(Video);
app.use(Payment);
app.use(ShoppingCart);


module.exports = app;