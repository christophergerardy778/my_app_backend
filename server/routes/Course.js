const Router = require("express").Router();
const CourseController = require("../controllers/CourseController.js");
const storage = require("../multer");
const multer = require("multer");


Router.route("/courses")
    .get(CourseController.getAllCourses);


Router.route("/course").post(
    multer({ storage} ).single("img"),
    CourseController.createCourse);


Router.route("/course/:userId/:courseId")
    .get(CourseController.infoCourse);


Router.route("/course/check/:userId/:courseId")
    .get(CourseController.checkCourse);



Router.route("/course")
    .get(CourseController.searchCourse);


Router.route("/my-courses/:userId")
    .get(CourseController.getMyCourses);

Router.route("/add-section")
    .post(CourseController.addSection);


module.exports = Router;