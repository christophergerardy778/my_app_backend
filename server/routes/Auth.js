const Router = require("express").Router();
const LoginController = require("../controllers/LoginController");
const RegisterController = require("../controllers/RegisterController");

Router.route("/login").post(LoginController.login);
Router.route("/register").post(RegisterController.register);

module.exports = Router;