const Router = require("express").Router();
const PaymentController = require("../controllers/PaymentController");

Router.route("/checkout").post(PaymentController.createIntent);

Router.route("/webhook").post((PaymentController.webHook));

Router.route("/save-payment").post(PaymentController.savePaymentIntent);


module.exports = Router;
