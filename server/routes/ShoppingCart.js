const Router = require("express").Router();
const ShoppingCartController = require("../controllers/ShoppingCartController");

Router.route("/shopping-cart").post(ShoppingCartController.addToShoppingCard);
Router.route("/shopping-cart").delete(ShoppingCartController.removeFromShoppingCart);
Router.route("/shopping-cart/:userId").get(ShoppingCartController.getShoppingCart);

module.exports = Router;