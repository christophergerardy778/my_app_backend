'use strict';
module.exports = (sequelize, DataTypes) => {
  const PaymentIntent = sequelize.define('PaymentIntent', {
    paymentIntent: DataTypes.STRING,
    shoppingCartId: DataTypes.INTEGER
  }, {});
  PaymentIntent.associate = function(models) {
    // associations can be defined here
  };
  return PaymentIntent;
};