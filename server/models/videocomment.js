'use strict';
module.exports = (sequelize, DataTypes) => {
  const VideoComment = sequelize.define('VideoComment', {
    videoId: DataTypes.INTEGER,
    comment: DataTypes.STRING,
    userId: DataTypes.INTEGER
  }, {});
  VideoComment.associate = function(models) {
    // associations can be defined here
  };
  return VideoComment;
};