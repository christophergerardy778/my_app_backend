'use strict';
module.exports = (sequelize, DataTypes) => {
  const RateUser = sequelize.define('RateUser', {
    courseId: DataTypes.INTEGER,
    studentId: DataTypes.INTEGER,
    rate: DataTypes.DOUBLE
  }, {});
  RateUser.associate = function(models) {
    // associations can be defined here
  };
  return RateUser;
};