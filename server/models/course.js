'use strict';
module.exports = (sequelize, DataTypes) => {
  const Course = sequelize.define('Course', {
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    instructorId: DataTypes.INTEGER,
    students: DataTypes.INTEGER,
    categoryId: DataTypes.INTEGER,
    imgId: DataTypes.INTEGER,
    rate: DataTypes.DOUBLE,
    price: DataTypes.DOUBLE
  }, {});
  Course.associate = function(models) {
    Course.hasOne(models.Category, {
      foreignKey: "id",
      sourceKey: "categoryId"
    });

    Course.hasOne(models.User, {
      foreignKey: "id",
      sourceKey: "instructorId"
    });

    Course.hasOne(models.CourseImg, {
      foreignKey: "id",
      sourceKey: "imgId"
    });

    Course.hasMany(models.Section, {
      foreignKey: "courseId",
      sourceKey: "id"
    });
  };
  return Course;
};