'use strict';
module.exports = (sequelize, DataTypes) => {
  const InShoppingCart = sequelize.define('InShoppingCart', {
    shoppingCartId: DataTypes.INTEGER,
    courseId: DataTypes.INTEGER
  }, {});
  InShoppingCart.associate = function(models) {
    // associations can be defined here
    InShoppingCart.hasOne(models.Course, {
      foreignKey: "id",
      sourceKey: "courseId"
    })
  };
  return InShoppingCart;
};