'use strict';
module.exports = (sequelize, DataTypes) => {
  const Rate = sequelize.define('Rate', {
    courseId: DataTypes.INTEGER,
    rate: DataTypes.DOUBLE
  }, {});
  Rate.associate = function(models) {
    // associations can be defined here
  };
  return Rate;
};