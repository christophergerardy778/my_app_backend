'use strict';
module.exports = (sequelize, DataTypes) => {
  const payment = sequelize.define('payment', {
    courseId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER
  }, {});
  payment.associate = function(models) {
    payment.hasOne(models.Course, {
      sourceKey: "courseId",
      foreignKey: "id"
    })
  };
  return payment;
};