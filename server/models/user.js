'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    lastName: DataTypes.STRING,
    gender: DataTypes.INTEGER,
    accountType: DataTypes.INTEGER,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    avatar: DataTypes.STRING,
    userName: DataTypes.STRING,
    cartId: DataTypes.INTEGER
  }, {
    defaultScope: {
      attributes: { exclude: ["createdAt", "updatedAt"] }
    }
  });
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};