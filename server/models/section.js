'use strict';
module.exports = (sequelize, DataTypes) => {
  const Section = sequelize.define('Section', {
    courseId: DataTypes.INTEGER,
    name: DataTypes.STRING
  }, {});
  Section.associate = function(models) {
    Section.hasMany(models.Video, {
      foreignKey: "sectionId",
      sourceKey: "id"
    })
  };
  return Section;
};