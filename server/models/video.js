'use strict';
module.exports = (sequelize, DataTypes) => {
  const video = sequelize.define('Video', {
    sectionId: DataTypes.INTEGER,
    name: DataTypes.STRING,
    publicId: DataTypes.STRING,
    url: DataTypes.STRING
  }, {});
  video.associate = function(models) {
    // associations can be defined here
    video.hasMany(models.VideoComment, {
      sourceKey: "id",
      foreignKey: "videoId" // TODO: Crear modelo comentarios pero de videos individuales
    });
  };
  return video;
};