'use strict';
module.exports = (sequelize, DataTypes) => {
  const CourseImg = sequelize.define('CourseImg', {
    courseId: DataTypes.INTEGER,
    publicId: DataTypes.STRING,
    url: DataTypes.STRING
  }, {});
  CourseImg.associate = function(models) {
    // associations can be defined here
  };
  return CourseImg;
};