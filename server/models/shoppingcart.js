'use strict';
module.exports = (sequelize, DataTypes) => {
  const ShoppingCart = sequelize.define('ShoppingCart', {
    userId: DataTypes.INTEGER,
  }, {});
  ShoppingCart.associate = function(models) {
    // associations can be defined here
    ShoppingCart.hasMany(models.InShoppingCart, {
      foreignKey: "shoppingCartId",
      sourceKey: "id"
    })
  };
  return ShoppingCart;
};