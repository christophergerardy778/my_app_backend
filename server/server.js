require("colors");

const router = require("./routes/Router");
const { PORT } = require("./env");
const express = require("express");
const morgan = require("morgan");
const clear = require("clear");
const cors = require("cors");
const app = express();

// MIDDLEWARE
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// ROUTER
app.use(router);

app.listen(PORT, () => {
    clear();
    console.log(`  SERVER ON PORT: ${PORT}  `.bgGreen);
});
